function [FLT,uni,acel,m_rel] = F_lor(q,m,E_loc,B_loc,x0,v0)
%% F_lor(q,m,E_tot,x0,v0,loc)
%This function determines the electric Lorentz force from an electric field
% E_tot [in V/m] on a (relativistic moving) particle with charge q [in C] 
%and restmass m [in kg] at the position vector x0 (a 2 by 1 vector with the
% cartesian coordinates in m]), moving with the velocity v0 [in m/s]. The
% loc input determinse the location of the particle in the E-field.
%Output: The electric Lorentz force, FLE, a 2 by 1 vector with the force in
%cartesian directions [in N]. The unity vector, uni, or direction of the 
%force. The acceleration, acel, of the particle in [m/s�].
%% 
c = 3e8;                              %Speed of light [in m/s]
uni = x0/norm(x0);                    %evaluating the unity vectors in cartesian coordinates
gamma = 1/sqrt(1-norm(v0)^2/c^2);     %Lorentz factor
m_rel = m*gamma;                      %Relativistic mass [in kg]
FLE = q.*E_loc.*uni;                  %evaluating the electrical component of the Lorentz force [in N]
FLB = q.*cross(v0,B_loc);             %evaluating the magnetic component of the Lorentz force [in N]
FLT = FLE + FLB;                      %Total Lorentz force [in N]
acel = FLT./m_rel;                    %evaluating the acceleration of the particle [in m/s�]
end