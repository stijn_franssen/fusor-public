%% plots
%Plot for my bachelor thesis.
clear all; close all; clc;
plotnumber = 1;                 %Variable for numbering plots
addpath('D:\Cloud\OneDrive\BachelorOpdracht\Simulaties\Plots\old\') %location of the data to be plotted
%% Contourplot of a crossection of the E-field
load('Electric field contour.mat')
figure(plotnumber)
hold on
contourf(Xax,Xax,E_tot(:,:,nx/2),'EdgeColor','none')                %contourplot of the electric field
colbar = colorbar('Direction','reverse');                 %Creating a ledgend
colbar.Label.String = 'Electric field strength (V/m)';
title('Crossection of the E-field'); xlabel('X coordinates (m)'); ylabel('Y coordinates (m)')
axis equal
hold off
plotnumber = plotnumber + 1;
%% Quiver plots of the B-field
% The crosssections look like cuspfields
load('2D Quiver.mat')
xlab = 'x (m)'; ylab = 'y (m)'; zlab = 'z (m)';
figure(plotnumber)
suptitle('Direction of the magnetic field, crossections')
subplot(1,3,1)
quiver(squeeze(Xp(:,:,round((nx-1)/2))),squeeze(Yp(:,:,round((nx-1)/2))),squeeze(Bx(:,:,round((nx-1)/2))),squeeze(By(:,:,round((nx-1)/2))),3) 
axis square; axis([-0.8 0.8 -.8 .8]);
xlabel(xlab); ylabel(ylab); 
subplot(1,3,2)
quiver(squeeze(Yp(:,round((nx-1)/2),:)),squeeze(Zp(:,round((nx-1)/2),:)),squeeze(By(:,round((nx-1)/2),:)),squeeze(Bz(:,round((nx-1)/2),:)),3)
axis square; axis([-0.8 0.8 -.8 .8]);
xlabel(ylab); ylabel(zlab);
subplot(1,3,3)
quiver(squeeze(Xp(round((nx-1)/2),:,:)),squeeze(Zp(round((nx-1)/2),:,:)),squeeze(Bx(round((nx-1)/2),:,:)),squeeze(Bz(round((nx-1)/2),:,:)),3)
axis square; axis([-0.8 0.8 -.8 .8]);
xlabel(xlab); ylabel(zlab);
plotnumber = plotnumber + 1;
figure(plotnumber)
quiver3(Xp,Yp,Zp,Bx,By,Bz,2)     %3D Quiver of the B-field
axis equal; 
xlabel(xlab); ylabel(ylab); zlabel(zlab); title('Direction of the magnetic field');
plotnumber = plotnumber + 1;
%% Path of one particle
load('Location 1 particle')
figure(plotnumber)
hold on
plot3(LOC(:,1),LOC(:,2),LOC(:,3))
plot3(0,0,0,'r*')
axis equal; axis([-R3 R3 -R3 R3 -R3 R3]); view([1,1,1]);
xlabel('x (m)'); ylabel('y (m)'); zlabel('z (m)'); title('Path of the particle')
hold off
plotnumber = plotnumber + 1;
%% Relativistic Energy of the particle
% As can be seen from the plot, the relativistic energy of the particle
% stays roughly constant over the course of the simulation. This indicates
% that the magnetic field does not add energy to the particle. This 
% validates the model.
load('Relativistic energy.mat')
figure(plotnumber)
hold on
plot(t,E_rel)
title('Relativistic energy of the particle with no electric field'); xlabel('Time (s)'); ylabel('Energy (J)')
hold off
plotnumber = plotnumber + 1;
%% Confinement time as function of magnet strength
% The confinement time seems to scale linearly with the strength of the
% magnets.
load('Confinement time as function of magnetstrength.mat')
figure(plotnumber)
ylim([0,2.2e-8])
hold on
plot(bmod,t_endm)
title('Confinement time as function of magnet strenght'); xlabel('Strenght of the magnets (A/m)'); ylabel('Confinement time (s)')
hold off
plotnumber = plotnumber + 1;
%% Path of the electrons with no B field
% With no B-field present, the electrons travel radially outward, away from
% the negative potential. This validates the model.
load('Path no E field.mat')
figure(plotnumber)                           %plotting the position of the particle
hold on
for ij = 1:np^3
    plot3(locs(:,1,ij),locs(:,2,ij),locs(:,3,ij))
end
plot3(0,0,0,'r*')
axis equal; axis([-R3 R3 -R3 R3 -R3 R3]); view([1 -1 0.3])
xlabel('x (m)'); ylabel('y (m)'); zlabel('z (m)'); 
% title('Path of the particle')
hold off
plotnumber = plotnumber + 1;
%% Path of electrons in high B field
load('Path of electrons in high B field.mat')
figure(plotnumber)
hold on
for ii = 1:np^3
    plot3(locs(:,1,ii),locs(:,2,ii),locs(:,3,ii))
end
plot3(0,0,0,'r*')
axis equal; axis([-R3 R3 -R3 R3 -R3 R3]); view([1 -1 0.3])
% title('Path of multiple particles'); 
xlabel('x (m)'); ylabel('y (m)'); zlabel('z (m)');
hold off
plotnumber = plotnumber + 1;
%% Strenght of the B-field at the x-axes
load('D:\Cloud\OneDrive\BachelorOpdracht\Simulaties\Bfield\06_Oct_2015_023835.mat')
figure(plotnumber);              %Magnetic field in the X-direction on the x-axes
plot(Xax,Bx(round(5*nx/10),:,round(nx/2)))                  %B (Y, X , Z)!!!!
plotnumber = plotnumber + 1;
title('B-field at the x-axes for cusp configuration'); xlabel('x (m)'); ylabel('Magnetic field strenght (T)')