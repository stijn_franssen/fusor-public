function [LOC,t_end,dc,x_f] = particle_path(t, nt, tstep, R3, Xax, xstep, x0, v0, q, m, E_tot, Bx, By, Bz)
%% particle_path
%This function traces out the path of a charged particle trough a magentic
%assisted fusor.
%Input: nt tstep t
% R3
% x0 v0, q, m
% Xax xstep
% E_tot
% Bx By Bz

%Simulation
% speed = zeros(1,nt);
% xcol = zeros(2,nt);
x0 = zeros(1,3);
loc = zeros(1,3);
LOC = zeros(nt,3);
dc = zeros(nt,1);
% mass = zeros(1,nt);
for tt = 1:nt
    if norm(x0)<=R3                                   %only particles inside the vacuum vessel will be simulated
        x0 = x0 + tstep.*v0;                          %evaluating the new location
        for ik = 1:3
            [~,loc(ik)] = min(abs(Xax-x0(ik)));
        end
%         loc = [find(Xax>=(x0(1)-xstep) & Xax<=(x0(1)+xstep)), find(Xax>=(x0(2)-xstep) & Xax<=(x0(2)+xstep)), find(Xax>=(x0(3)-xstep) & Xax<=(x0(3)+xstep))];        %translating the location to coordinates on the grid
        LOC(tt,:) = x0;                         %Logging the position of the particle
        
        dc(tt) = norm(x0);        
        E_loc = E_tot(loc(1),loc(2),loc(3));          %E-field at the location of the particle
        B_loc = [Bx(loc(2),loc(1),loc(3)), By(loc(2),loc(1),loc(3)), Bz(loc(2),loc(1),loc(3))];
        [~,~,acel,~] = F_lor(q,m,E_loc,B_loc,x0,v0);     %evaluating forces and accelerations
        v0 = v0 + tstep.*acel;                        %evaluating new speed
        dc(nt) = norm(loc);                 %distance from the center of the particle
        x_f = x0;
%         speed(:,tt) = norm(v0);
%         mass(:,tt) = m_rel;                           %recording the different values of mass of the particle

        %             if mod(tt,100) == 0            %Record the location of the colision     round(tcolt/tstep
        %                  xcol(:,tt) = [Xax(loc(1)) Xax(loc(2))];
        %                 plot(Xax(loc(1)),Xax(loc(2)),'og')            %Plotting the location of the particle
        %             end
    else
        
        
        %             formatSpec = 'The particle has reached the vacuum vessel after %d s, at, x=%.3g m, Y=%.3g m and Z=%.3g.';
        %             sprintf(formatSpec,t(tt),x0(1),x0(2),x0(3))         %printing end location text
        break
    end
end
t_end = t(tt);                   %total confinement time
% LOC = LOC(1:tt-1,:);             %Removing excess zero values of Location
% LOC(LOC==0) = [];
% dc(dc==0)=[];                    %Removing exces zero values of distance to the center.
% speed(speed==0) = [];            %Removing excess zero values of speed
% xcol(xcol==0) = [];              %Removing excess zero values of location of colisions
% mass(mass==0) = [];              %Removing excess zero values of mass
end