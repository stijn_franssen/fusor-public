%% Data analysis
% This file analyses the data aquierd 
clear variables; close all; clc;
%% Importing data
% Imports the data: t time [s]. pr pressure [Pa]. pr_set the set pressure
% [Pa]. V the voltage [kV]. I the current [mA]. NPR neutron production
% rate.
[t,pr,pr_set,V,I,NPR] = importfile('D:\Universiteit\BachelorOpdracht\Matlab\Data\ShotNr_33_date_23-Oct-2015_time_12-12-39.dat');
t = t-t(1);                 % Setting t(1) = 0 s
%
figure(1)
hold on
plot(pr.*100)
plot(pr_set.*100,'--')
plot(V,'y')
plot(I,'g')
plot(NPR,'r')
legend('Pressure','set pressure','Voltage','Current','NPR')
hold off

%% Extracting mean values 
t_int = 286;                     % Start of constant regime
t_end = 569;                    % End of constant regime
% dpr = std(pr(t_int:t_end))      % STD pressure
% pr_b = mean(pr(t_int:t_end))    % Mean pressure
% % V_b = mean(V(t_int:t_end))      % Mean Voltage
% % dV = std(V(t_int:t_end))        % STD Voltage
% dur = round(t(t_end)-t(t_int))  % Duration of constant regime
% NPR_b = mean(NPR(t_int:t_end))  % Mean NPR during constant regime
% dNPR = std(NPR(t_int:t_end))    % Standard deviation of NPR during constant regime
% I_b = mean(I(t_int:t_end))      % Mean current during constant regime
% dI = std(I(t_int:t_end))        % Standard deviation of current during constant regime





%% Comparing to data sets
[t2,pr2, pr_set2,V2,I2,NPR2] = importfile('D:\Universiteit\BachelorOpdracht\Matlab\Data\ShotNr_40_date_23-Oct-2015_time_14-14-43.dat');
t_int2 = 333;
t_end2 = 575;

NPR = NPR.*6.03e4;
NPR2 = NPR2.*6.03e4;


figure(3)
hold on
plot(NPR(283:569),'r')
plot(NPR2,'b')
% title('Neutron production rate'); 
xlabel('time (s)'); ylabel('Neutrons per second')
legend('Without magnets','With magnets')
hold off

% Moving average filter
movaf = 50;         % Ammount of points to filter
b  = (1/movaf).*ones(1,movaf);        % creating filter function
NPR_filt = filter(b,1,NPR);           % Aplying filter
NPR2_filt = filter(b,1,NPR2);
figure(2)
hold on
ylim([0 5.5e5])
plot(t(t_int2:t_end),NPR_filt(t_int2:t_end),'r')
plot(t(t_int2:t_end2),NPR2_filt(t_int2:t_end2),'b')
% title('Neutron production rate');
xlabel('time (s)'); ylabel('Neutrons per second')
legend('Without magnets','With magnets','location','SE')
hold off

%% Breakdown voltage
clear variables
load('paschen.mat') % Loads voltage [kV] and pressure [Pa] with and without
% magnets V_wo, P_wo, V_mag, P_mag.

for ii = 1:7              % avering out multiple measurements
    strt = (ii-1)*5+1;              % First element to average
    fnl = (ii-1)*5+5;           % Final element to average
    V_wo_bar(ii) = mean(V_wo(strt:fnl));            % Mean value of Voltage
    dV_wo(ii) = std(V_wo(strt:fnl));                % Error of Voltage
    P_wo_bar(ii) = mean(P_wo(strt:fnl));            % Mean value of Pressure
end
dV_wo([4 7]) = 0.5/sqrt(5);
dP_wo = (0.002/sqrt(5)).*ones(1,7);                % Error in Pressure

V_mag_bar = [
    mean(V_mag(1:5))
    mean(V_mag(6:11))
    mean(V_mag(12:16))
    mean(V_mag(17:23))
    mean(V_mag(24:28))
    mean(V_mag(29))
    mean(V_mag(30))
    mean(V_mag(31))
    mean(V_mag([32 34]))
    ];      % Averages for Voltage with magnets
dV_mag = [
    std(V_mag(1:5))./sqrt(5)
    std(V_mag(6:11))./sqrt(5)
    std(V_mag(12:16))./sqrt(4)
    std(V_mag(17:23))./sqrt(6)
    std(V_mag(24:28))./sqrt(4)
    0.5
    .5
    .5
    std(V_mag([32 34]))./sqrt(2)
    ];      % Errors for Voltage with magnets
P_mag_bar = [
    mean(P_mag(1:5))
    mean(P_mag(6:11))
    mean(P_mag(12:16))
    mean(P_mag(17:23))
    mean(P_mag(24:28))
    mean(P_mag(29))
    mean(P_mag(30))
    mean(P_mag(31))
    mean(P_mag([32 34]))
    ];      % Averages for Voltage with magnets
dP_mag = [
    0.002./sqrt(5)
    0.002./sqrt(5)
    0.002./sqrt(4)
    std(P_mag(17:23))./sqrt(6)
    std(P_mag(24:28))./sqrt(4)
    0.002
    0.002
    0.002
    0.002./sqrt(2)
    ];      % Errors for Voltage with magnets


figure(4)
hold on
plot(P_wo_bar,V_wo_bar,'r*')
plot(P_mag_bar,V_mag_bar,'+')
errorbox(P_wo_bar,V_wo_bar,dP_wo,dV_wo)
errorbox(P_mag_bar,V_mag_bar,dP_mag,dV_mag)
xlabel('Pressure (Pa)'); ylabel('Voltage (kV)')
% title('Breakdown voltage')
legend('Without magnets','With magnets')
hold off


