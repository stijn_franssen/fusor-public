%% Simulation parle path
%Simulation of a charged particle in a spherical magnetic assisted inertial
%electrostatic confinement device (IEC). This m-file calls the functions
%e_field to set up the electrical field inside the IEC and F_lor to
%determine the force upon the particle and the associated acceleration.
clear all; close all; clc;
%% Loading B-field
load('D:\Cloud\OneDrive\BachelorOpdracht\Simulaties\Bfield\06_Oct_2015_023835.mat')
%Loading the magnetic field: nx spatial resolution, Bx, By and Bz the
%magnetic field, R2 the outer radius, Xax axes of the grid, X, Y and Z the
%grid, magnets the amount of magnets used.
R3 = R2;                    %Renaming the radius of the vaccuum vessel

%% Parameters
%Physical constants
mu0 = 4e-7 * pi;            %Magnetic constant [N/A�]
ec = 1.602e-19;             %Elementary charge [in C]
k = 1.380e-23;              %Boltzmann constant [in J/K]
c = 3e8;                              %Speed of light [in m/s]

%Particle parameters
x0 = [0.1 -0.1 0.01];        %starting position of the particle [in m]
v0 = [0 0 1e7];             %starting velocity of the particle [in m/s]
q = -ec;                    %charge of the particle [in C]
m = 9.109e-31;              %mass of the particle [in kg]
%mass electron 9.109e-31, mass proton 1.672e-27

%Electric field parameters
R1 = 0.05;                   %Radius of the inner grid [in m]
R2 = 0.4;                   %Radius of the outer grid
% R3 = 0.005;               %Radius of the vaccuum vessel [[from B-field]]
Vol = 4/3*pi*R2^3;          %volume of the outer sphere [in m�]
V1 = -1e3;                  %Voltage of the inner grid [in V]
V2 = 0.0;%1*V1;               %voltage of the outer grid [in V] (if V2 = 0, the outer grid will be assumed to not be present.
V3 = 0;
dV1 = V1-V2;                %The effective potential between the inner and outer grid
dV2 = V2-V3;                %The effective potential between the outer grid and the vaccuum vesel

%Plasma paramters
T = 300;                    %Temperature of the plasma? [in K]
Pr = 0.1;                   %pressure [in Pa], for the Fusor between 0.1 and 0.5 Pa
% Ener = abs(V_eff.*ec);    %Energy or temperature of the plasma [J]
dens = Pr*Vol/(T*k);        %density of the plasma [in /m�]
sig = pi*(0.05e-9)^2;       %crossection of the neutral atoms? [in m�]
tcolt = 3.09e-7;            %theoretical colision time?? [in s]

%Simulation parameters
tend = 1e-6;                %Total simulation time
nt = 10000;                 %Time resolution
tstep = tend/(nt-1);        %time stepsize
t = 0:tstep:tend;           %time scale
xstep = R3/((nx-1));        %stepsize


%Space simulation parameters
% nx = 10;                   %space resolution (maximum for E ~800, maximum for B ~400) [[from B-field]]
% W = ones(1,nx^3);           %4th dimensional translation parameter
% Xax = -R2:2*xstep:R2;       %Axes of the grid [[from B-field]]
% [X,Y,Z] = meshgrid(Xax);    %Setting up the meshgrid [[from B-field]]


%% E field
%setting E field
[E_tot,Xax] = e_field(R1,R2,R3,nx,Xax,xstep,dV1,dV2);    %Setting the E-field
% 
% 
% %
% figure(1)                                                 %starting the figure
% hold on
% contourf(Xax,Xax,E_tot(:,:,nx/2),'EdgeColor','none')                %contourplot of the electric field
% colbar = colorbar('Direction','reverse');                 %Creating a ledgend
% colbar.Label.String = 'Electric field strength (V/m)';
% loc = [find(Xax>=(x0(1)-xstep) & Xax<=(x0(1)+xstep)) find(Xax>=(x0(2)-xstep) & Xax<=(x0(2)+xstep)) find(Xax>=(x0(3)-xstep) & Xax<=(x0(3)+xstep))];        %translating the location to coordinates on the grid
% plot(Xax(loc(1)),Xax(loc(2)),'r*')                        %Plotting the point
% xlabel('X coordinates (m)')
% ylabel('Y coordinates (m)')
% axis equal;
% % Simulation
% speed = zeros(1,nt);
% mass = zeros(1,nt);
% xcol = zeros(2,nt);
% LOC = zeros(nt,3);
% % while norm(x0)>R1 && q<1                                  %an electron that is released inside the smaller sphere will not be simulated.
% for tt = 1:nt
%     if norm(x0)<=R3                                   %only particles inside the vacuum vessel will be simulated
%         x0 = x0 + tstep.*v0;                          %evaluating the new location
%         loc = [find(Xax>=(x0(1)-xstep) & Xax<=(x0(1)+xstep)) find(Xax>=(x0(2)-xstep) & Xax<=(x0(2)+xstep)) find(Xax>=(x0(3)-xstep) & Xax<=(x0(3)+xstep))];        %translating the location to coordinates on the grid
%         LOC(tt,:) = Xax(loc);                         %Logging the position of the particle
%         plot(Xax(loc(1)),Xax(loc(2)),'r.')            %Plotting the location of the particle
%         
%         E_loc = E_tot(loc(1),loc(2),loc(3));          %E-field at the location of the particle
%         B_loc = [Bx(loc(2),loc(1),loc(3)), By(loc(2),loc(1),loc(3)), Bz(loc(2),loc(1),loc(3))];
%         [FLT,uni,acel,m_rel] = F_lor(q,m,E_loc,B_loc,x0,v0);     %evaluating forces and accelerations
%         v0 = v0 + tstep.*acel;                        %evaluating new speed
%         speed(:,tt) = norm(v0);                       %recording the different values of speed of the particle
%         mass(:,tt) = m_rel;                           %recording the different values of mass of the particle
%         %             if mod(tt,100) == 0            %Record the location of the colision     round(tcolt/tstep
%         %                  xcol(:,tt) = [Xax(loc(1)) Xax(loc(2))];
%         %                 plot(Xax(loc(1)),Xax(loc(2)),'og')            %Plotting the location of the particle
%         %             end
%     else
%         formatSpec = 'The particle has reached the vacuum vessel after %d s, at, x=%.3g m, Y=%.3g m and Z=%.3g.';
%         sprintf(formatSpec,t(tt),x0(1),x0(2),x0(3))         %printing end location text
%         tt = tt-1;
%         break
%     end
% end
% %     break
% % end
% hold off
% 
% LOC = LOC(1:tt-1,:);             %Removing excess zero values of Location
% speed(speed==0) = [];            %Removing excess zero values of speed
% xcol(xcol==0) = [];              %Removing excess zero values of location
% mass(mass==0) = [];              %Removing excess zero values of mass
% 
% E_rel = (mass.*c^2)./(sqrt(1-speed.^2/c^2));
% 
% 
% v_mean = 2.*speed.*sqrt(2/pi);                     %maxwell-Boltzmann mean speed
% colt = mean(1./(v_mean(round(nx/5):end).*dens.*sig));      %mean electron neutral colision time
% % t_col = mean(colt(nt/5:end))
% coltdif = abs(tcolt-colt);
% % if coltdif > tcolt/100
% %     disp('The theoretical collision time may be poorly chosen. The
% %     estimated collision time is') disp(colt)
% % else
% %     if isempty(xcol)
% %         disp('No collisions have occurred.')
% %     else
% %         disp('The folowing amount of collisions have occurred:')
% %         disp(length(xcol))
% %     end
% % end
% 
% figure(2)
% hold on
% plot(t(1:length(speed)),speed)
% title('V(t)')
% xlabel('time'); ylabel('E_rel')
% hold off
% 
% figure(3)
% plot(t(1:tt),E_rel)
% title('Total relativistic energy as function of time')
% speed(end)
% std(speed)
% E_rel(end)
% std(E_rel)
%  
% 
% 
% % % v_mean(end)
% 
% % figure(3);              %Magnetic field in the X-direction on the x-axes
% % plot(Bx(round(5*nx/10),:,round(nx/2)))                  %B (Y, X , Z)!!!!
% 
% figure(4)                           %plotting the position of the particle
% hold on
% plot3(LOC(:,1),LOC(:,2),LOC(:,3))
% plot3(0,0,0,'r*')
% axis equal; axis([-R3 R3 -R3 R3 -R3 R3]);
% xlabel('x (m)'); ylabel('y (m)'); zlabel('z (m)'); title('Path of the particle')
% hold off

%% Multiple particles
np = 3;                            %the total number of particles will be np^np
nB = 1;                     %amount of magnetic fieldstrengts to be simulated
bmod = linspace(0,.9e6,nB); %Gainfactor for modifying the magneticfield to a strenght between 0 and .9e6 A/m
nE = 1;                   %amount of voltages to be simulated
emod =10;% linspace(1,100,nE);  %Gainfactor for modyfining the electric field to a voltage between -1e3 and -100e3 V
%% placement of the particles
% full grid
% partplace = linspace(R1,R3,np);             %creates a [1 np] dimensional array for particles to spawn (if np=2 --> [a b])
% rightcol = repmat(partplace,1,np);              %repeats the array horizontaly (creates a [1 np*2] array)
% rightcol = [rightcol -rightcol -rightcol rightcol];
% ar2 = repmat(partplace,np,1);               %repeats the array vertically (creates a [np 2] array
% midcol = reshape(ar2,1,np*np);          %flattens ar2 (if np=2 [a a b b])
% midcol = [midcol -midcol midcol -midcol];
% ext = vertcat(midcol,rightcol);             %places ar2 beneath ar1 [2 np*2]
% ext = repmat(ext,1,np);            %repeat ext to [2 niip*4]
% ext = [ext ext];
% ar3 = repmat(partplace,np*np,1);             %repeat v vertically (creates a [2*np 2] array
% leftcol = reshape(ar3,1,np*np*np);       %flattens ar3 (if np=2 [a a a a b b b b])
% leftcol = [leftcol leftcol leftcol leftcol -leftcol -leftcol -leftcol -leftcol];
% x0 = vertcat(leftcol,ext).';     %vercat places ar3 underneath ext, .' reshapes the array to a [np*4 3]
% one octant
partplace = linspace(R1,R3,np);             %creates a [1 np] dimensional array for particles to spawn (if np=2 --> [a b])
ar1 = repmat(partplace,1,np);              %repeats the array horizontaly (creates a [1 np*2] array)
ar2 = repmat(partplace,np,1);               %repeats the array vertically (creates a [np 2] array
ar2 = reshape(ar2,1,np*np);          %flattens ar2 (if np=2 [a a b b])
ext = vertcat(ar2,ar1);             %places ar2 beneath ar1 [2 np*2]
ext = repmat(ext,1,np);             %repeat ext to [2 np*4]
ar3 = repmat(partplace,np*np,1);             %repeat v vertically (creates a [2*np 2] array
ar3 = reshape(ar3,1,np*np*np);       %flattens ar3 (if np=2 [a a a a b b b b])
x0 = vertcat(ar3,ext).';     %vercat places ar3 underneath ext, .' reshapes the array to a [np*4 3]
%
tnp = length(x0);               %True amount of particles
v0 = zeros(tnp,3);              %starting speed of the particles
locs = zeros(nt,3,tnp,nB);         %Location of the particles
t_end = zeros(nt,1);            %Time at which a particle hits the outer wall (or end of simulation time)
t_endm = zeros(nB,1);           %Mean end time
dc = zeros(tnp,nt);             %Distance of the particle to the center
R_he = sqrt(10).*R1;              %The ions created outside of this radius will have at least the amount of energy requierd
% Efield = zeros(nx,nx,nx,nE);
HE_t = zeros(1,nB);
x_f = zeros(3,tnp,nB);
Efield(:,:,:) = E_tot.*emod(1);

for ij = 1:nB                 %For different Magnetic/Electric Fields
    tic
    BBx = Bx.*(bmod(ij)/.9e6);        %changes the x-component strength of the B-field
    BBy = By.*(bmod(ij)/.9e6);        %changes the y-component strength of the B-field
    BBz = Bz.*(bmod(ij)/.9e6);        %changes the z-component strength of the B-field
    for ii = 1:tnp                   %Track the motion of each particle
        [locs(:,:,ii,ij),~,dc(ii,:),x_f(:,ii,ij)] = particle_path(t, nt, tstep, R3, Xax, xstep, x0(ii,:), v0(ii,:), q, m, Efield(:,:,:), BBx(:,:,:), BBy(:,:,:), BBz(:,:,:)); %locs(:,:,ii,ij),t_end(ii),dc(ii,:)x_f(:,ii,ij)
        %         t_end(t_end == 0) = [];             %Removing exces zeros
        %         t_endm(ij) = mean(t_end);
%         locs(locs==0) = [];
        
    end
    %      t_conf = mean(t_end);        %mean confinement time
    tot_greater = dc>R_he;          %Only records the particles in the outer shell (defined by R_he)
    tot_greater(tot_greater==0) = [];   %cut out 0 values
    HE_t(ij) = t(ceil(sum(tot_greater(:))/tnp));       %time in high enery region
    toc
end

% end


%%
% locs(locs==0) = nan;

% Figures
nfig = 1;
for ii = 1:nB
    figure(nfig)                           %plotting the position of the particle
    hold on
    for ij = 1:np^3
        plot3(locs(:,1,ij,ii),locs(:,2,ij,ii),locs(:,3,ij,ii))
    end
    plot3(0,0,0,'r*')
    axis equal; axis([-R3 R3 -R3 R3 -R3 R3]);
    xlabel('x (m)'); ylabel('y (m)'); zlabel('z (m)'); title('Path of the particle')
    hold off
    nfig = nfig + 1;
end
figure(nfig)
plot(bmod,HE_t)




%%
%junk code
% trying to remove particles that originate outside the fusor
%for ii = 1:np^3
%     disp(norm(result(ii,:)));
%     if norm(result(ii,:)) < R3
%         x0(ii,:) = result(ii,:);
%     end
% end
% x0 = x0(any(x0,2),:);
% for ii = 1:np^3
% %     disp(norm(result(ii,:)))
%     if norm(result(ii,:)) < R3
%         x0(ii,:) = result(ii,:);
%     end
% end





