%% plotting data acquired with neutron measurements
clear variables; close all; clc;
format short
load('D:\Universiteit\BachelorOpdracht\Matlab\Data\extractedata.mat');
% Load variables (NPR) NeutronProductionRate (counts/s), dNPR standard
% deviation of NPR, I current (mA), dI standard deviation of I, P pressure
% (Pa), shot the shotnumber (shots between and including 36:45 are with
% magnets), t_end the duration of the measurement (s), V the voltage (V).
%%
% from count/s to neutrons per second
% NPR = NPR.*6.03e4;
% dNPR = dNPR.*6.03e4;

% % Sorting according to pressure or voltage
% % % % the pressure is P = 0.19 Pa
% I_19 = [I(4) I(5) I(7) I(9) I(27)];                 % Current without magnets
% NPR_19 = [NPR(4) NPR(5) NPR(7) NPR(9) NPR(27)];     % NPR without magnets
% I_19_M = [I(11) I(13) I(14) I(16:19).' I(24)];        % Current with magnets
% NPR_19_M = [NPR(11) NPR(13) NPR(14) NPR(16:19).' NPR(24)]; % NPR with magnets
% 
% figure(1)
% hold on
% plot(I_19,NPR_19,'dr')
% plot(I_19_M,NPR_19_M,'*b')
% xlabel('Current (mA)'); ylabel('NPR (neutrons/s)')
% legend('Without magnets','With magnets','Location','NW')
% hold off
% 
% % % % % The voltage is V = 40 kV
% P_zonder = P([4:9 25:28]);          % Without magnets 
% P_mag = P([10:19 23]);              % With magnets     
% NPR_zonder = NPR([4:9 25:28]);  % Without magnets
% NPR_mag = NPR([10:19 23]);  % With magnets
% n_zonder = length(P_zonder);        % Number of measurements without magnets
% n_mag = length(P_mag);          % Number of measurements with magnets
% dP_zonder = 0.001.*ones(1,n_zonder);          % miniature error in pressure
% dP_mag = 0.001.*ones(1,n_mag);           % miniature error in pressure
% dNPR_zonder = (1/sqrt(n_zonder)).*dNPR([4:9 25:28]);  % Without magnets
% dNPR_mag = (1/sqrt(n_mag)).*dNPR([10:19 23]);        % With magnets
% 
% figure(2)
% hold on
% axis([0.17 0.22 0 6e5])
% plot(P([4:9 25:28]),NPR([4:9 25:28]),'dr')
% plot(P([10:19 23]),NPR([10:19 23]),'*b')
% 
% for n=1:n_zonder                   % error boxes without magnets
%     x1=P_zonder(n)-dP_zonder(n);
%     y1=NPR_zonder(n)-dNPR_zonder(n);
%     w1=2*dP_zonder(n);
%     h1=2*dNPR_zonder(n);
%     rectangle('Position',[x1,y1,w1,h1])
% end
% for n=1:n_mag                   % error boxes with magnets
%     x1=P_mag(n)-dP_mag(n);
%     y1=NPR_mag(n)-dNPR_mag(n);
%     w1=2*dP_mag(n);
%     h1=2*dNPR_mag(n);
%     rectangle('Position',[x1,y1,w1,h1])
% end
% 
% xlabel('Pressure (Pa)'); ylabel('NPR (neutrons/s)')
% legend('Without magnets','With magnets','Location','NW')
% hold off

%% Taking only P = 0.19 and V = 40 kV
NPR_wo = [5.63 2.26; 5.57 2.28; 6.2 2.5; 6.72 2.56; 7.3 2.61].*6.03e4;  % NPR without magnets and the error
n_wo = length(NPR_wo);          % Number of measurements without magnets
NPR_mag = [7.14 2.47; 7.47 2.58; 7.53 2.42; 7.83 2.63; 8.23 2.9; 7.04 2.38; 8.31 2.91].*6.03e4; % NPR with magnets and the error
n_mag = length(NPR_mag);        % Number of measurements with magnets
NPR_wo_m = mean(NPR_wo(:,1))       % mean value without magnets
dNPR_wo = (1/sqrt(n_wo))*mean(NPR_wo(:,2))     % error
NPR_mag_m = mean(NPR_mag(:,1))         % Mean value with magnets
dNPR_mag = (1/sqrt(n_mag))*mean(NPR_mag(:,2))      % error





























