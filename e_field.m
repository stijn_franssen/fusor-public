function [E_tot,Xax] = e_field(R1,R2,R3,nx,Xax,xstep,dV1,dV2)
%% e_field
%This function determines the electric field in three dimensions
%E_tot [in V/m]) in a fusor with inner grid radius R1 outer grid radius R2 
%and a vacuum vessel with radius R3 [in m], where R1<R2<R3 with a spatial
%resolution of nx, and an applied voltage V_eff [in V]. If dV2 = 0 (so the
%voltage of the outer grid = 0) it is assumed that the outer grid is not
%present.
%Output: E_tot (the electric field in a nx by nx matrix), Xax a vector of
%length nx that can be used for plots. 
%%
%Constants
eps0 = 8.854187871e-12;             %Vacuum permittivity [in F/m]
r = 0:2*xstep:2*R3;                 %r coordinates

if dV2 == 0                         %if the outer grid is not present
    q1 = dV1/(1/R1-1/R3);            %the amount of charge on the inner grid
    
else
    q1 = dV1/(1/R1-1/R2);          %the amount of charge on the inner grid
    q2 = dV2/(1/R2-1/R3);           %The amount of charge on the outer grid
end
%%
%calculating the electric field
E_r = zeros(1,nx);                       %Creating empty array
if dV2 == 0
    for ii = 1:nx
        if (r(ii) >= R1 && r(ii) < R3)       %there is only an electric field between the spheres
            E_r(ii) = q1./(r(ii)).^2;     %The electric field, 1 dimensional.
        else
            E_r(ii) =0;                      %there is only an electric field between the spheres
        end
    end
else
    for ii = 1:nx
        if (r(ii) >= R1 && r(ii) < R2)       %there is only an electric field between the spheres
            E_r(ii) = q1./(r(ii)).^2;     %The electric field, 1 dimensional.
        elseif (r(ii) >= R2 && r(ii) < R3)
            E_r(ii) = q2./(r(ii)).^2;     %The electric field, 1 dimensional.
        else
            E_r(ii) =0;                      %there is only an electric field between the spheres
        end
    end
end
    %%
    [X,Y,Z] = meshgrid(1:(nx/2));                     %Setting up the meshgrid
    %Creating E-field sphere/circle
    E = E_r(floor(sqrt(X.^2+Y.^2+Z.^2)));                %Translating the 1D data to a sphere, just 1 octant
    E_tot = flip([rot90(E,2),flip(E,1);flip(E,2),E],3);     %Creating half a sphere
    % X = flip([-1.*rot90(E,2),flip(E,1);flip(E,2),E],3);     %Creating half a sphere
    
    E_tot(:,:,nx/2+1:nx) = flip(E_tot,3);           %creating full sphere
%% plots
% figure(10)
% plot(Xax(floor(nx/2):end),E_r(1:ceil(nx/2)+1))
% title('The electric field from the center 1D')

end
