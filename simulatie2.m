%% Simulatie 2
% simulating particles moving in a fusor.
clear variables; close all; clc;
%% Constants and parameters
R1 = 0.05;                   % Radius of the inner grid [in m]
R3 = .25;                    % Radius of the vacuum vessel [in m]

%% Magnet configuration
W = 1;                       % Necessary for translation matrix
Mj = 1;                      % Magnet Strenght
Dv = R3/sqrt(3);             % Distance from center to projected vertex magnets
mag = [                      % magnet paramters, x y z strenght, x y z location
    Mj 0 0   R3 0 0          % 1 cuboctahedron face magnet
    0 Mj 0   0 R3 0          % 2
    -Mj 0 0  -R3 0 0         % 3
    0 -Mj 0  0 -R3 0         % 4
    0 0 Mj   0 0 R3          % 5
    0 0 -Mj   0 0 -R3        % 6
    Mj/3 Mj/3 Mj/3  Dv  Dv  Dv     %1  Vertex magnets cuboctahedron
    Mj/3 Mj/3 Mj/3  -Dv Dv  Dv     %2
    Mj/3 Mj/3 Mj/3  -Dv -Dv Dv     %3
    Mj/3 Mj/3 Mj/3  Dv  -Dv Dv     %4
    Mj/3 Mj/3 Mj/3  -Dv Dv  -Dv    %5
    Mj/3 Mj/3 Mj/3  Dv  Dv  -Dv    %6
    Mj/3 Mj/3 Mj/3  -Dv -Dv -Dv    %7
    Mj/3 Mj/3 Mj/3  Dv  -Dv -Dv    %8
    ];
%% Electric field configuration
V1 = -10e3;                  % Voltage of the inner grid [in V]
V2 = 0; %1*V1;               % Voltage of the outer grid [in V] (if V2 = 0, the outer grid will be assumed to not be present.
V3 = 0;                      % Voltage of  the vaccuum vessel
dV1 = V1-V2;                % The effective potential between the inner and outer grid
dV2 = V2-V3;                % The effective potential between the outer grid and the vaccuum vesel









%% Zooi
% Verifi�ren vorm van de sterkte op de xas

nx = 1000;
Xax = linspace(-.99*R3,.99*R3,nx);
B = zeros(nx,3);
Btot = zeros(nx,3);

for ii = 1:nx
    x0 = [Xax(ii) 0 0];
    B(ii,:) = bstrenght(mag,x0);
end
plot(Xax,B(:,1))