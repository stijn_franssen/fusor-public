%Function E = e_strength()
%% Electrical field strength
x0 = [0.05 0 0];
r = norm(x0);

q1 = dV1/(1/R1-1/R3);            %the amount of charge on the inner grid



if (r >= R1 && r < R3)       %there is only an electric field between the spheres
    E_r(ii) = q1/r^2;     %The electric field, 1 dimensional.
else
    E_r(ii) =0;                      %there is only an electric field between the spheres
end