function [LOC,t_end,x_end,SPEED] = part_path(nt,t,tstep,R3,x0,v0,q1,magnets,N_m,Lm,Rm,N_el,c,m,q)
LOC = zeros(nt,3);                      % Logging the location of the particle
for tt = 1:nt
    if norm(x0)<=R3                                   % Only particles inside the vacuum vessel will be simulated
        [acel] = F_lor2(x0,v0,q1,magnets,N_m,Lm,Rm,N_el,c,m,q);     % Determining the accelaration 
        v0 = v0 + tstep.*acel;                        % Evaluating new speed
        x0 = x0 + tstep.*v0;                          % Evaluating the new location
        LOC(tt,:) = x0;                               % Logging the location of the particle
    else
        x_end = x0;                                   % The end location of the particle
        t_end = t(tt);                                % The lifetime of the particle
        LOC(tt,:) = x0;                               % Logging the location of the particle
        LOC(LOC ==0) = nan;                           % Removing excess zeros
        break
    end
end