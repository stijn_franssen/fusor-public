function [LOC,t_end,x_end,SPD,x_int,n_col] = part_path_boris(nt,t,tstep,R3,x0,v0,q1,magnets,N_m,Lm,Rm,N_el,m,q,freq_max,alpha)
%% This functions determinse the path of a charged particle in a B and E field with the Boris Method
% A particle will be simulated in a magnetic assisted fusor.
% Input: nt number of time steps. t the time scale [s]. tstep the time step
% for the simulation.
% R3 the radius of the vaccuum vessel [m]. x0 the starting position of the
% particle in cartesian coordinates [m]. v0 the starting velocity of the
% particle [m/s]. q1 the charge on the.


c = 3e8;                                % Speed of light [m/s]
LOC = zeros(nt,3);                      % Logging the location of the particle
u_p = v0;                             % initial velocity, v^(n-�)
bf = (q*tstep)/(2*m);             % Boris method factor [C s]
SPD = zeros(nt,3);                  % Logging of the velocity of the particle
x_end = [0 0 0];
%% Collision settings
t_col = (0-1./freq_max*log(rand));         % The time at which the first collision has to be evaluated (if rand element is removed, remove the -!) 
[~, in] = min(abs(t-t_col));            % t_col mapped to t
alpha_max = max(alpha);
n_col= 0;                             % Ammount of collisions
x_int = zeros(3,100);                  % Record the location of the collision
for tt = 1:nt                         % For every timestep
    r = norm(x0);                        % Distance to the center [m]
    uni = x0./r;                         % Unity vector in cartesian coordinates
    if r<=R3                                   % Only particles inside the vacuum vessel will be simulated
        %% Evaluation of the electric and magnetic field
        E_tot = q1/r^2;                        % The electric field [V/m]
        Bx = 0; By = 0; Bz = 0;                % The magnetic field in x, y and z direction [A/m]
        for n=1:N_m                 % For every magnet compute the magnetic field and add it
            [Xm,Ym,Zm] = coord_transSP(x0(1),x0(2),x0(3),magnets(n,2),magnets(n,3),magnets(n,4),1,1,1,1,magnets(n,5),magnets(n,6),magnets(n,7));
            
            [BMx,BMy,BMz] = compute_magnet2(Xm(:),Ym(:),Zm(:),Lm,magnets(n,1),Rm,N_el);
            
            [B_x,B_y,B_z] = field_trans(BMx,BMy,BMz,1,1,1,1,magnets(n,5),magnets(n,6),magnets(n,7));
            Bx=Bx+B_x; By=By+B_y; Bz=Bz+B_z;
        end
        B_tot = [Bx,By,Bz];
        %% Boris method particle pusher
        % See Particle Pushing Methods by Daan van Vugt// Particle
        % simulation of plasmas, Verboncoeur
        u_min = u_p + bf.*E_tot.*uni;           % Equation 7
        gamma = sqrt(1+(norm(u_min)/c)^2) ;   % Lorentz factor
        t_vec = B_tot./norm(B_tot).*tan(bf./gamma.*B_tot); % Equation 11
        s_vec = (2*t_vec)./(1+dot(t_vec,t_vec));     % Additional vector
        u_acc = u_min + cross(u_min,t_vec);         % Equation 8
        u_plus = u_min + cross(u_acc,s_vec);    % Equation 9
        u_f = u_plus + bf.*E_tot.*uni;          % Equation 10
        x0 = x0 + u_f.*tstep;                   %
        
        x_end = x0;
        %% Collisions See Computational Plasma Physics by The Plasimo Team and S. Nijdam, version 2014, section 8.3.3
        if tt == in             % If the collision time has been reached, calculate a collision
            u_f = (u_p+u_f)./2;         % The velocity at the present time step (not half time step)
            gamma = sqrt(1+(norm(u_f)/c)^2) ;   % Lorentz factor
            e_kin = (gamma-1)*m*c^2;            % Relativistic kinectic energy
            %%
            
            if (alpha_max*rand<=e_kin)              % if the maximum energy is smaller than the energy of the particle at that moment
                n_col = n_col+1;                    % Increment the amount of collisions
                randnum = 2.*rand(1,3) -1;           % Creates a vector of three random values between [-1,1]
                normrand = randnum/norm(randnum);   % Normalizes the random vector
                u_p = norm(u_f).*normrand;          % Creates a new random velocity with the same magnitude as before
                x_int(:,n_col) = x0.';                % Record the location of the collision
%                 disp('true collision')
            else
%                 disp('non true collision')
            end
            t_col = (t_col-1e-1/freq_max*log(rand));    % The time at which the first collision needs to be evaluated 
            [~,in] = min(abs(t-t_col));            % t_col mapped to t, 
        else
            u_p = u_f;                      %The new past velocity is the current future velocity
        end
        
        LOC(tt,:) = x0;                               % Logging the location of the particle
        SPD(tt,:) = u_p;                              % Logging of the velovity of the particle
    else
        %         LOC(tt,:) = x0;                               % Logging the location of the particle
        SPD(tt,:) = u_p;
        SPD(SPD ==0) = nan;
        %         disp('The particle has hit the wall.')
        break
    end
end
% The end location of the particle
t_end = t(tt);                                % The lifetime of the particle
LOC(LOC ==0) = nan;                           % Removing excess zeros
SPD(SPD ==0) = nan;
% x_int = x_int.';

