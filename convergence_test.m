%% Convergence test for boris particle pusher part_path_boris.m 
% Parameters needed for part_path_boris.m are set in the function simrunner.m
clear variables; close all; clc;
format short
%% Nog te doen: meer punten, cftool gebruiken om aan te tonen dat het voornamelijk kwadratisch is. 
% Misschien de convergentie waarde er af halen en dan de fout iets of zo.
% Normaliter wordt het weergegeven in een loglogplot

R3= 0.25;           % Radius of the vaccuum vessel
n = 1;            % amount of points to be plotted
deltatmin = -11;  % minimum delta t
deltatmax = -10;   % maximum delta t
deltat = logspace(deltatmin,deltatmax,n);   % Delta t range
x_end = zeros(3,n);         % End locations of the particles [allocation]

for ii = 1:n                % For every delta t, run the simulation
    tic
    x_end(:,ii) = simrunner(deltat(ii));
    toc
end

figure(1)                       
title('Endlocations of the particles')
hold on
axsize = 2*R3;
axis equal; axis([-axsize axsize -axsize axsize -axsize axsize])
for ii = 1:n                % For every particle plot it's end location
    plot3(x_end(1,ii),x_end(2,ii),x_end(3,ii),'.');
end
hold off

figure(2)
hold on
title('Convergence test')
xlabel('\Deltat')
ylabel('x end position of the particle')
plot(deltat,x_end(1,:))
hold off

% save('D:\Cloud\OneDrive\BachelorOpdracht\Simulaties\Plots\convergence','deltat','x_end')