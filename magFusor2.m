clear variables; close all; clc;
%% Clear memory
format long;

%% Constants
mu0 = 4e-7 * pi;    % Vacuum permeability
Lm = 0.03;         % Length magnet [m]
Rm = 0.045;         % Radius magnet [m]

R2 = 0.25;
nx = 5;           %200 is kind of a minimum for non-jagged paths
xstep = R2/(nx-1);
r = 0:2*xstep:2*R2;    % Spatial coordinate [m]

%% Settings
W = ones(1,nx^3);           %4th dimensional translation parameter
xstep = R2/((nx-1));        %stepsize
Xax = -R2:2*xstep:R2;       %Axes of the grid
[X,Y,Z] = meshgrid(Xax);    %Setting up the meshgrid
%Grid settings
xmin = -R2;
xmax = R2;
ymin = xmin;
ymax = xmax;
zmin = xmin;
zmax = xmax;

% %Grid spacing
dx = 2*xstep;
dy = dx;
dz = dx;
Mj = .9e6;             %Coercive field strength [A/m]
Df = R2+Lm/2+2*xstep;        %Distance from the center to the face magnets
Dv = (Df)/sqrt(3);    %Distance from center to projected vertex magnets
magnets =[                      %Placing of the magnets [Strenght Xtranslation Ytranslation Ztranslation phi psi theta]
    Mj	Df	0   0 0 pi/2 0          %1 Face magnets cuboctahedron         
%     Mj	0 Df 0 0 0 -pi/2           %2
%     Mj	-Df 0  0 0 -pi/2 0         %3     
%     Mj	0 -Df  0 0 0 pi/2          %4
%     -Mj	0 0 Df 0 0 0               %5
%     Mj	0 0 -Df 0 0 0              %6
%     Mj  Dv  Dv  Dv  pi/4    0 pi/4              %1  Vertex magnets cuboctahedron
%     Mj  -Dv Dv  Dv  -pi/4   0 pi/4              %2
%     Mj  -Dv -Dv Dv  -3*pi/4 0 pi/4              %3
%     Mj  Dv  -Dv Dv  3*pi/4  0 pi/4              %4
%     Mj  -Dv Dv  -Dv -pi/4   0 3*pi/4            %5
%     Mj  Dv  Dv  -Dv pi/4    0 3*pi/4            %6
%     Mj  -Dv -Dv -Dv -3*pi/4 0 3*pi/4            %7
%     Mj  Dv  -Dv -Dv 3*pi/4  0 3*pi/4            %8
];
N_m = length(magnets(:,1));    % number of magnets
N_el = 10;        % number of slices per magnet


% Magnet spatial settings
j = zeros(1,N_m);       %Pre-allocating vectors
tx = zeros(1,N_m);
ty = zeros(1,N_m);
tz = zeros(1,N_m);
phi = zeros(1,N_m);
psi = zeros(1,N_m);
theta = zeros(1,N_m);

for ii = 1:N_m          %Create arrays per magnet info
    j(ii) = magnets(ii,1);
    tx(ii) = magnets(ii,2);
    ty(ii) = magnets(ii,3);
    tz(ii) = magnets(ii,4);
    phi(ii) = magnets(ii,5);
    psi(ii) = magnets(ii,6);
    theta(ii) = magnets(ii,7);
end

%% Magnet solution
% Bx=zeros(1,1,1); By=Bx; Bz=Bx;
% xs = 0.05; ys = 0; zs=0;
% [X, Y, Z] = meshgrid(xs, ys, zs);

% [X, Y, Z] = [0.05, 0, 0];
for n=1:N_m
    tic
    [Xm,Ym,Zm] = coord_trans(Xax,Xax,Xax,tx(n),ty(n),tz(n),nx,W,theta(n),psi(n),phi(n));
    %t=cputime;
    [BMx,BMy,BMz] = compute_magnet2(Xm(:),Ym(:),Zm(:),j(n),Lm,Rm,N_el);
    %cputime-t
    [B_x,B_y,B_z] = field_trans(BMx,BMy,BMz,nx,W,theta(n),psi(n),phi(n));
    Bx=Bx+B_x; By=By+B_y; Bz=Bz+B_z;
    toc
end


% Analytical solution for Bz on the axis of the magnet
%B_MOA=(j*mu0./2).*(((zs+(Lm./2))./sqrt((zs+(Lm./2)).^2+Rm.^2))-((zs-(Lm./2))./sqrt((zs-(Lm./2)).^2+Rm.^2)));
%% Saving workspace
datetime=datestr(now);                       %Creating date string
datetime=strrep(datetime,':','');            %Deleting colon
datetime=strrep(datetime,'-','_');           %Replace minus sign with underscore
datetime=strrep(datetime,' ','_');           %Replace space with underscore
% save(['D:\Cloud\OneDrive\Bachelor Opdracht\Simulaties\Bfield\' datetime '.mat'],'nx','Bx','By','Bz','R2','Xax','X','Y','Z','magnets')
%% Plots
% [Xp,Yp,Zp]=meshgrid(Xax);
% % figure(1); quiver(squeeze(Xp(:,:,round((nx-1)/2))),squeeze(Yp(:,:,round((nx-1)/2))),squeeze(Bx(:,:,round((nx-1)/2))),squeeze(By(:,:,round((nx-1)/2))),3) 
% % axis equal; xlabel('x (m)'); ylabel('y (m)'); title('Field of magnet [T]');
% % figure(2); quiver(squeeze(Yp(:,round((nx-1)/2),:)),squeeze(Zp(:,round((nx-1)/2),:)),squeeze(By(:,round((nx-1)/2),:)),squeeze(Bz(:,round((nx-1)/2),:)),3)
% % axis equal; xlabel('y (m)'); ylabel('z (m)'); title('Field of magnet [T]');
% % figure(3); quiver(squeeze(Xp(round((nx-1)/2),:,:)),squeeze(Zp(round((nx-1)/2),:,:)),squeeze(Bx(round((nx-1)/2),:,:)),squeeze(Bz(round((nx-1)/2),:,:)),3)
% % axis equal; xlabel('x (m)'); ylabel('z (m)'); title('Field of magnet [T]');
% figure(4); quiver3(Xp,Yp,Zp,Bx,By,Bz,2)     %3D Quiver of the B-field
% axis equal; xlabel('x (m)'); ylabel('y (m)'); zlabel('z (m)'); title('Field of magnet [T]');


% figure(5);              %Magnetic field in the X-direction on the x-axes
% plot(Bx(round(5*nx/10),:,round(nx/2)))                  %B (Y, X , Z)!!!!
% figure(9)
% quiver(Yp(:,:,20),Xp(:,:,20),By(:,:,20),Bx(:,:,20))
% figure(8)
% Q = sqrt(squeeze(Bx(1,:,:)).^2+squeeze(By(:,1,:)).^2+squeeze(Bz(:,1,:)).^2);
% contour(squeeze(Yp(:,1,:)),squeeze(Zp(:,1,:)),Q,40);

% [sy,sz] = meshgrid(ymin:Rm/3:ymax,zmin:Rm/3:zmax);
% Xt = (squeeze(Xp(:,:,1)));
% Y2t = (squeeze(Yp(:,:,1)));
% Yt = transpose(squeeze(Yp(:,1,:)));
% Zt = transpose(squeeze(Zp(:,1,:)));
% Bxt = (squeeze(Bx(:,:,1)));
% By2t = (squeeze(By(:,:,1)));
% Byt = transpose(squeeze(By(:,1,:)));
% Bzt = transpose(squeeze(Bz(:,1,:)));
% 
% figure(6)
% streamline(stream2(Yt,Zt,Byt,Bzt,sy,sz));
% 
% Xg=ceil(length(Xax)/2);
% Yg=ceil(length(Xax)/2);
% figure(7)
% hold on
% plot(zs,squeeze(B_z(Yg,Xg,:)))
% plot(zs,B_MOA','o');
% hold off








































% %% Simulation variables
% N_el = 1000;        % number of slices per magnet
% tstep = 1e-11;               %  Delta t, time stepsize
% tend = 1e-6;                % Total simulation time
% t = 0:tstep:tend;           % time scale
% nt = length(t);
% 
% 
% %% Constants and parameters
% R1 = 0.05;                   % Radius of the inner grid [m]
% R3 = .25;                    % Radius of the vacuum vessel [m]
% c = 3e8;                     % Speed of light [m/s]
% m = 9.109e-31;               % Rest mass of the particle [kg]
% q = -1.602e-19;               % Charge of the particle [C]
% %% Electric field configuration
% V1 = -10e3;                  % Voltage of the inner grid [in V]
% V2 = 0; %1*V1;               % Voltage of the outer grid [in V] (if V2 = 0, the outer grid will be assumed to not be present.
% V3 = 0;                      % Voltage of  the vaccuum vessel
% dV1 = V1-V2;                % The effective potential between the inner and outer grid
% % dV2 = V2-V3;                % The effective potential between the outer grid and the vaccuum vesel
% q1 = dV1/(1/R1-1/R3);            %the amount of charge on the inner grid
% 
% %% Magnet configuration
% Mj = 0.9e6;             % Coercive field strength [A/m]   default: 0.9e6;
% Lm = 0.03;             % Length magnet [m]
% Rm = 0.0225;            % Radius magnet [m]
% Df = R3-Lm/2;          % Distance from the center to the face magnets
% Dv = (Df)/sqrt(3);     % Distance from center to projected vertex magnets
% % Magnet spatial settings
% magnets =[                      %Placing of the magnets [Strenght Xtranslation Ytranslation Ztranslation phi psi theta]
%     Mj	Df	0   0   0       pi/2    0         %1 Face magnets cuboctahedron
%     Mj	0   Df  0   0       0       -pi/2     %2
%     Mj	-Df 0   0   0       -pi/2   0         %3
%     Mj	0   -Df 0   0       0       pi/2      %4
%     -Mj	0   0   Df  0       0       0         %5
%     Mj	0   0   -Df 0       0       0         %6
%     Mj    Dv  Dv  Dv  pi/4    0       pi/4      %1  Vertex magnets cuboctahedron
%     Mj    -Dv Dv  Dv  -pi/4   0       pi/4      %2
%     Mj    -Dv -Dv Dv  -3*pi/4 0       pi/4      %3
%     Mj    Dv  -Dv Dv  3*pi/4  0       pi/4      %4
%     Mj    -Dv Dv  -Dv -pi/4   0       3*pi/4    %5
%     Mj    Dv  Dv  -Dv pi/4    0       3*pi/4    %6
%     Mj    -Dv -Dv -Dv -3*pi/4 0       3*pi/4    %7
%     Mj    Dv  -Dv -Dv 3*pi/4  0       3*pi/4    %8
%     ];
% [N_m, ~] = size(magnets);       % Amount of magnets used
% %% Actual calculation
% Bx = 0; By = 0; Bz = 0;                % The magnetic field in x, y and z direction [A/m]
% for n=1:N_m                 % For every magnet compute the magnetic field and add it
%     [Xm,Ym,Zm] = coord_trans(x0(1),x0(2),x0(3),magnets(n,2),magnets(n,3),magnets(n,4),1,1,1,1,magnets(n,5),magnets(n,6),magnets(n,7));
%     
%     [BMx,BMy,BMz] = compute_magnet2(Xm(:),Ym(:),Zm(:),Lm,magnets(n,1),Rm,N_el);
%     
%     [B_x,B_y,B_z] = field_trans(BMx,BMy,BMz,1,1,1,1,magnets(n,5),magnets(n,6),magnets(n,7));
%     Bx=Bx+B_x; By=By+B_y; Bz=Bz+B_z;
% end