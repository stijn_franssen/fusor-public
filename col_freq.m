%% Collision Frequency
% The collision frequency between an electron and a non-ionised gas
% particle.
clear variables; close all; clc;
%% Constants
m0 = 9.11e-31;          % Electron rest mass [kg]
kb = 1.380e-23;              % Boltzmann constant [in J/K]
amu = 1.661e-27;        % Atomic mass unit [kg]
% R = 8.314;              % Gas constant [J/mol/K]
%% Variables
% to set
m_n = 2;                % mass number of the gas particle
T = 300;                % Temperature of the plasma [K]
R3 = 0.25;              % Radius of the vacuum vessel [m]
Pr = 0.1;               % pressure [in Pa], for the Fusor between 0.1 and 0.5 Pa
r = 5.29177e-11;        % Radius of the gas particle [m] �Bohr Radius?
% 
sigma = pi*r^2;         % Effective crossesction of the gas particle [m�]
nd = Pr/(kb*T);         % Number density of the gas
% V = 3*pi/4*R3^3;        % Volume of the vacuum vessel [m�]
M_m = m_n*amu;          % Mass of the gas particle [kg]
% rho = (Pr*M_m)/(T*R);   % Density of the gas [kg/m3]
gamma = 1/sqrt(1-norm(v)^2/c^2)