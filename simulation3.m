%% Simulation 3
% simulating particles moving in a fusor.
clear variables; close all; clc;
%% Simulation variables
N_el = 100;        % number of slices per magnet
tstep = 1e-11;               %  Delta t, time stepsize
tend = 1e-7;                % Total simulation time
t = 0:tstep:tend;           % time scale
nt = length(t);             % Ammount of timesteps


%% Constants and parameters
R1 = 0.05;                   % Radius of the inner grid [m]
R3 = .25;                    % Radius of the vacuum vessel [m]
c = 3e8;                     % Speed of light [m/s]
k = 1.380e-23;               % Boltzmann constant [in J/K]
m = 9.109e-31;               % Rest mass of the particle [kg]
q = -1.602e-19;              % Charge of the particle [C]
T = 300;                     % Temperature of the plasma? [in K]
Pr = 0.5;                    % pressure [in Pa], for the Fusor between 0.1 and 0.5 Pa
%% Electric field configuration
V1 = -10e3;                  % Voltage of the inner grid [in V]
V2 = 0; %1*V1;               % Voltage of the outer grid [in V] (if V2 = 0, the outer grid will be assumed to not be present.
V3 = 0;                      % Voltage of  the vaccuum vessel
dV1 = V1-V2;                 % The effective potential between the inner and outer grid
% dV2 = V2-V3;                 % The effective potential between the outer grid and the vaccuum vesel
q1 = dV1/(1/R1-1/R3);        %the amount of charge on the inner grid

%% Magnet configuration
Mj = 0.9e6;             % Coercive field strength [A/m]   default: 0.9e6;
Lm = 0.03;              % Length magnet [m]
Rm = 0.0225;            % Radius magnet [m]
ft = 12.5e-3;           % Thickness of the fusor [m]
Df = R3-Lm/2+ft;        % Distance from the center to the face magnets
Dv = (Df)/sqrt(3);      % Distance from center to projected vertex magnets
% Magnet spatial settings
magnets =[                      %Placing of the magnets [Strenght Xtranslation Ytranslation Ztranslation phi psi theta]
    Mj	Df	0   0   0       pi/2    0         %1 Face magnets cuboctahedron
    Mj	0   Df  0   0       0       -pi/2     %2
    Mj	-Df 0   0   0       -pi/2   0         %3
    Mj	0   -Df 0   0       0       pi/2      %4
    -Mj	0   0   Df  0       0       0         %5
    Mj	0   0   -Df 0       0       0         %6
    Mj    Dv  Dv  Dv  pi/4    0       pi/4      %1  Vertex magnets cuboctahedron
    Mj    -Dv Dv  Dv  -pi/4   0       pi/4      %2
    Mj    -Dv -Dv Dv  -3*pi/4 0       pi/4      %3
    Mj    Dv  -Dv Dv  3*pi/4  0       pi/4      %4
    Mj    -Dv Dv  -Dv -pi/4   0       3*pi/4    %5
    Mj    Dv  Dv  -Dv pi/4    0       3*pi/4    %6
    Mj    -Dv -Dv -Dv -3*pi/4 0       3*pi/4    %7
    Mj    Dv  -Dv -Dv 3*pi/4  0       3*pi/4    %8
    ];
[N_m, ~] = size(magnets);       % Amount of magnets used
%% Collision settings
load('ratecoef.mat');                 % Loads the ratecoeficient of hydrogen (alpha [m�/s]) as a function of energy (rateE [J])
dens = Pr/(T*k);                 % density of the plasma [in /m�]
colfreq = alpha.*dens;             % The frequency of electrons-hydrogen collisions [1/s]
freq_max = max(colfreq);           % The maximum frequency of electrons-hydrogen collisions [1/s]
%% Simulating 1 particle
% x0 = [0.05 0.01 0];          % Initial position particle [m]
% v0 = [0 0 0];                % Initial velocity particle [v]
% tic
% [LOC,t_end,x_end,SPD,x_int,n_col] = part_path_boris(nt,t,tstep,R3,x0,v0,q1,magnets,N_m,Lm,Rm,N_el,m,q,freq_max,alpha);
% toc
% disp(x_end)
% %% Simulating multiple particles
magsim = 50;       %amount of magnets to simulate
t_conf = zeros(1,magsim);      % allocating mean confinement time
% actual code
    np = 10;                     % np specifies the amount of steps from R1 to R3
    x0 = multpartgen(np,R1,R3./2);   % generates a list of particles linspaced(R1,R3,np) in the postive xyz space.
    [tnp,~] = size(x0);                     % True number of particles to be simulated
    v0 = zeros(tnp,3);          % Generates zeors for the starting velocity of the particles
        magnets(:,1) = magnets(:,1).*(1/100);          % chaging the magnet strenght

for ij = 38:40          %for 100 differtent magnetic fields                 1:magsim
    magnetsr(:,1) = magnets(:,1).*(ij);          % chaging the magnet strenght
    magnetsr(:,2:7) = magnets(:,2:7);            % other magnet stuf 
    
    t_end = zeros(tnp,1);       % The time at which each particle hits the outer wall (or the end time of the simulation)
    % LOC = zeros(nt,3,tnp);      % The location of the particles during the simulation
    x_end = zeros(tnp,3);       % The end location of the particles
    x_int = zeros(3,100,tnp);       % The locations of the collisions
    tic
    parfor ii = 1:tnp           % For every particle determine its path, end time, end location and its speed
        
        [~,t_end(ii),~,~,~,~] = part_path_boris(nt,t,tstep,R3,x0(ii,:),v0(ii,:),q1,magnetsr,N_m,Lm,Rm,N_el,m,q,freq_max,alpha);          %LOC(:,:,ii),t_end(ii),x_end(ii,:),~,x_int(:,:,ii),n_col(ii)
        
    end
    toc
    x_int = reshape(x_int,3,[]);
    x_int(:,~any(x_int,1)) = [];
    t_end = t_end(t_end ~= 0);   % Removing excess zeros from the end time
    t_conf(ij) = mean(t_end);
end



%%
save('D:\Universiteit\BachelorOpdracht\Matlab\Data\t_conf','t_conf')
figure(1)
plot(t_conf)
% figure(1)
% xlab = 'x (m)'; ylab = 'y (m)'; zlab = 'z (m)';
% hold on
% plot3(x_end(:,1),x_end(:,2),x_end(:,3),'.')
% axis equal; axis([-R3 R3 -R3 R3 -R3 R3]); view(3);
% xlabel(xlab); ylabel(ylab); zlabel(zlab);
% hold off
% 
% figure(2)
% hold on
% axis equal; axis([-R3 R3 -R3 R3 -R3 R3]); view(3);
% for ii = 1:tnp
%     plot3(LOC(:,1,ii),LOC(:,2,ii),LOC(:,3,ii))
% end
% hold off
% %%
% figure(1)
% axis equal; axis([-R3 R3 -R3 R3 -R3 R3])
% hold on
% % % plot3(LOC(:,1),LOC(:,2),LOC(:,3))
% 
% plot3(x_int(1,:),x_int(2,:),x_int(3,:),'rd')
% title('Boris method')
% hold off
% % figure(2)
% % title('Regular method')
% % plot3(LOCo(:,1),LOCo(:,2),LOCo(:,3))
% % axis equal; axis([-R3 R3 -R3 R3 -R3 R3])
% disp(t_end);
% disp(x_end);
% 
% % [nspd,~] = size(SPD); 
% % vnorm = zeros(nspd,1);
% % for ii = 1:nspd
% vnorm(ii) = norm(SPD(ii,:));
% end
% figure(3)
% plot(vnorm);
% plot3(LOC
%
%
%
%         plot3(locs(:,1,ij,ii),locs(:,2,ij,ii),locs(:,3,ij,ii))