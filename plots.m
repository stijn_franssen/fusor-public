%% plots
%Plot for my bachelor thesis.
clear variables; close all; clc;
plotnumber = 1;                 %Variable for numbering plots
addpath('D:\Cloud\OneDrive\BachelorOpdracht\Simulaties\Plots') %location of the data to be plotted
%%
xlab = 'x (m)'; ylab = 'y (m)'; zlab = 'z (m)';
%% One particle
load('oneparticle.mat')
figure(plotnumber)
hold on
plot3(LOC(:,1),LOC(:,2),LOC(:,3))
title('The path of 1 particle determind by the Boris Method')
axis equal; axis([-R3 R3 -R3 R3 -R3 R3]); view(3);
xlabel(xlab); ylabel(ylab); zlabel(zlab);
plotnumber = plotnumber +1;
%% Convergence
load('convergence.mat')
figure(plotnumber)
hold on
suptitle('Convergence test')
ylab = {'x';'y';'z'};
for ii = 1:3
    subplot(1,3,ii)
    plot(deltat,x_end(ii,:))
    xlabel('\Deltat [s]')
    ylabel([ylab(ii);'end position [m]'])
end
hold off
plotnumber = plotnumber +1;
%% Rate coeficient
load('ratecoef.mat')
figure(plotnumber)
semilogx(rateE,alpha)
hold on
title('Rate coeficient of hydrogen as function of the temperature')
xlabel('Energy in Joule'); ylabel('Rate coeficient in m�/s')
hold off
plotnumber = plotnumber +1;
