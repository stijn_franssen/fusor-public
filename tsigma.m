%% Ratecoeficient of hydrogen 
clear variables; close all; clc;
% Data acquired from: http://open.adas.ac.uk/detail/adf07/ionelec/ionelec_szd][h.dat
% Temperature (rateE) originally in eV, converted to Joule
% Rate coeficient (alpha) of hydrogen originally in cm�/s, converted to m�/s
rateE = 1.602177e-19.*[1.00E+00	2.00E+00	3.00E+00	4.00E+00	5.00E+00	7.00E+00 1.00E+01	1.50E+01	2.00E+01	3.00E+01	4.00E+01	5.00E+01 7.00E+01	1.00E+02	1.50E+02	2.00E+02	3.00E+02	4.00E+02 5.00E+02	7.00E+02	1.00E+03	2.00E+03	5.00E+03	1.00E+04];
alpha = 1e-6.*[7.57E-15	9.74E-12	1.17E-10	4.25E-10	9.48E-10	2.47E-09 5.32E-09	1.01E-08	1.41E-08	1.99E-08	2.37E-08	2.63E-08 2.92E-08	3.09E-08	3.14E-08	3.09E-08	2.94E-08	2.80E-08 2.66E-08	2.45E-08	2.22E-08	1.77E-08	1.27E-08	9.69E-09];

figure(1)
semilogx(rateE,alpha)
hold on
title('Rate coeficient of hydrogen as function of the temperature')
xlabel('Energy in Joule'); ylabel('Rate coeficient in m�/s')
hold off


% save('D:\Cloud\OneDrive\Bachelor Opdracht\Matlab\ratecoef','rateE','alpha')