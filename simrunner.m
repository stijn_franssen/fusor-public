function  [x_end] = simrunner(tstep)
%% Simulation 3
% simulating particles moving in a fusor.
% clear variables; close all; clc;
%% Simulation variables
N_el = 1000;        % number of slices per magnet
% tstep = 1e-10;               %  Delta t, time stepsize
tend = 1e-6;                % Total simulation time
% nt = round(tend/tstep+1);         % Time resolution
% tstep = tend/(nt-1);        % time stepsize
t = 0:tstep:tend;           % time scale
nt = length(t);             

%% Constants and parameters
R1 = 0.05;                   % Radius of the inner grid [m]
R3 = .25;                    % Radius of the vacuum vessel [m]
c = 3e8;                     % Speed of light [m/s]
m = 9.109e-31;               % Rest mass of the particle [kg]
q = -1.602e-19;               % Charge of the particle [C]
%% Electric field configuration
V1 = -10e3;                  % Voltage of the inner grid [in V]
V2 = 0; %1*V1;               % Voltage of the outer grid [in V] (if V2 = 0, the outer grid will be assumed to not be present.
V3 = 0;                      % Voltage of  the vaccuum vessel
dV1 = V1-V2;                % The effective potential between the inner and outer grid
% dV2 = V2-V3;                % The effective potential between the outer grid and the vaccuum vesel
q1 = dV1/(1/R1-1/R3);            %the amount of charge on the inner grid

%% Magnet configuration
Mj = 0.9e6;             % Coercive field strength [A/m]   default: 0.9e6;
Lm = 0.03;             % Length magnet [m]
Rm = 0.0225;            % Radius magnet [m]
Df = R3-Lm/2;          % Distance from the center to the face magnets
Dv = (Df)/sqrt(3);     % Distance from center to projected vertex magnets
% Magnet spatial settings
magnets =[                      %Placing of the magnets [Strenght Xtranslation Ytranslation Ztranslation phi psi theta]
    Mj	Df	0   0   0       pi/2    0         %1 Face magnets cuboctahedron
    Mj	0   Df  0   0       0       -pi/2     %2
    Mj	-Df 0   0   0       -pi/2   0         %3
    Mj	0   -Df 0   0       0       pi/2      %4
    -Mj	0   0   Df  0       0       0         %5
    Mj	0   0   -Df 0       0       0         %6
    Mj    Dv  Dv  Dv  pi/4    0       pi/4      %1  Vertex magnets cuboctahedron
    Mj    -Dv Dv  Dv  -pi/4   0       pi/4      %2
    Mj    -Dv -Dv Dv  -3*pi/4 0       pi/4      %3
    Mj    Dv  -Dv Dv  3*pi/4  0       pi/4      %4
    Mj    -Dv Dv  -Dv -pi/4   0       3*pi/4    %5
    Mj    Dv  Dv  -Dv pi/4    0       3*pi/4    %6
    Mj    -Dv -Dv -Dv -3*pi/4 0       3*pi/4    %7
    Mj    Dv  -Dv -Dv 3*pi/4  0       3*pi/4    %8
    ];
[N_m, ~] = size(magnets);       % Amount of magnets used
%% Simulating 1 particle
x0 = [0.05 0.01 0];          % Initial position particle [m]
v0 = [0 0 0];                % Initial velocity particle [v]
% tic
[~,~,x_end,~] = part_path_boris(nt,t,tstep,R3,x0,v0,q1,magnets,N_m,Lm,Rm,N_el,m,q);
