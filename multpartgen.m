function x0 = multpartgen(np,R1,R3)
%% Creates mutilple particles in a square array between R1 and R3
% np specifies the amount of steps from R1 to R3

% R1 = .05;
% R2 = .25;
% np = 2;

partplace = linspace(R1,R3,np);             %creates a [1 np] dimensional array for particles to spawn (if np=2 --> [a b])
ar1 = repmat(partplace,1,np);              %repeats the array horizontaly (creates a [1 np*2] array)
ar2 = repmat(partplace,np,1);               %repeats the array vertically (creates a [np 2] array
ar2 = reshape(ar2,1,np*np);          %flattens ar2 (if np=2 [a a b b])
ext = vertcat(ar2,ar1);             %places ar2 beneath ar1 [2 np*2]
ext = repmat(ext,1,np);             %repeat ext to [2 np*4]
ar3 = repmat(partplace,np*np,1);             %repeat v vertically (creates a [2*np 2] array
ar3 = reshape(ar3,1,np*np*np);       %flattens ar3 (if np=2 [a a a a b b b b])
x0 = vertcat(ar3,ext).';     %vercat places ar3 underneath ext, .' reshapes the array to a [np*4 3]