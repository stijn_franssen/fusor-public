function [Btot] = bstrenght(mag,x0)
%% b strenght
[n_mag,~] = size(mag);
Btot = zeros(1,3);
for ii = 1:n_mag
    r_pr = x0-mag(ii,4:6);
    B =       1e-7.*(((3*r_pr).*dot(mag(ii,1:3),r_pr))./(norm(r_pr)^5)-mag(ii,1:3)./(norm(r_pr)^3));
    Btot = Btot + B;
end