function [ acel ] = F_lor2(x0,v0,q1,magnets,N_m,Lm,Rm,N_el,c,m,q)
%% This function calculates the Lorentz force on a particle.
%% Evaluating factors
r = norm(x0);                        % Distance to the center [m]
uni = x0./r;                         % Unity vector in cartesian coordinates
gamma = 1/sqrt(1-norm(v0)^2/c^2);     % Lorentz factor
m_rel = m*gamma;                      % Relativistic mass [kg]
%% Electric and magnetic field
E_tot = q1/r^2;                        % The electric field [V/m]
Bx = 0; By = 0; Bz = 0;                % The magnetic field in x, y and z direction [A/m]
for n=1:N_m                 % For every magnet compute the magnetic field and add it
        [Xm,Ym,Zm] = coord_trans(x0(1),x0(2),x0(3),magnets(n,2),magnets(n,3),magnets(n,4),1,1,1,1,magnets(n,5),magnets(n,6),magnets(n,7));
        
        [BMx,BMy,BMz] = compute_magnet2(Xm(:),Ym(:),Zm(:),Lm,magnets(n,1),Rm,N_el);
        
        [B_x,B_y,B_z] = field_trans(BMx,BMy,BMz,1,1,1,1,magnets(n,5),magnets(n,6),magnets(n,7));
        Bx=Bx+B_x; By=By+B_y; Bz=Bz+B_z;
end
B_tot = [Bx,By,Bz];
%% The Lorenz force
FLE = q.*E_tot.*uni;                  % evaluating the electrical component of the Lorentz force [in N]
FLB = q.*cross(v0,B_tot);             % evaluating the magnetic component of the Lorentz force [in N]
FLT = FLE + FLB;                      % Total Lorentz force [in N]
acel = FLT./m_rel;                    % evaluating the acceleration of the particle [in m/s�]
